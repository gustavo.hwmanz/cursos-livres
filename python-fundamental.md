# Python Fundamental

## Sobre o curso

* Carga Horária: 8h
* Mês: 02/2019
* Responsável: [Lucas Souza](@souzaluuk)
* Professor: [Filipe Saraiva](@filipesaraiva)

## Ementa do Curso

1. Motivação: Por que estudar Python?
    1. Utilizações
1. Instalação, ambiente, execução e editores
1. Variáveis básicas
    1. Tipagem dinâmica
    1. Variáveis numéricas
    1. Variáveis lógicas
        1. Operadores lógicos
    1. String
1. E/S de dados
    1. input
    1. output
1. Operadores condicionais
    1. if
    1. else
    1. elif
    1. operador ternário
1. Operadores de repetição
    1. while
    1. for
        1. for contador
        1. for em listas e conjuntos
    1. Repetições aninhadas
1. Funções
1. Bibliotecas
1. Variáveis avançadas
    1. list
    1. dict
    1. tuple
    1. set
1. Operações com arquivos
1. Classes e objetos
