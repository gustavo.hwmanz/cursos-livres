# Curso de Introdução ao Linux
### Sobre o curso
* CH: 8h
* Mês: 01/2019
* Responsável: [Lucas Souza](@souzaluuk)
* Professor: [Filipe Saraiva](@filipesaraiva)
### Ementa do Curso
1. Introdução ao Curso
      1. Principais Conceitos
			1. Software Livre
            1. Open Source
            1. Licenças
            1. Comunidades
      1. Unix
      1. GNU/Linux (SO/Kernel)
			1. Sistema Operacional
			1. Kernel
			1. Interpretador
			1. Permissões e propriedades
			1. Estrutura de diretórios
	  1. Distribuições
			1. Debian
			1. Fedora
			1. OpenSUSE
			1. Ubuntu
	  1. Interfaces Gráficas
			1. Gnome
			1. KDE
	  1. Ferramentas no GNU/Linux
			1. Ferramentas de ecritório
			1. Browser
			1. IDE
			1. Outros
1. Instalação Linux
	  1. Escolha de uma distribuição
      1. Processo de Instalação
			1. Criação e configuração de uma VM
			1. Etapas de instalação
			1. Particionamento de disco
      1. Instalação de Programas
            1. Meio Gráfico
            1. Terminal
1. Shell (Comandos)
      1. Antes de começar
      1. Principais Comandos
            1. Manual do Bash
            1. Arquivos e Diretórios
				1. Listagem
				1. Navegação e Busca
				1. Escrita
				1. Leitura
				1. Exclusão
            1. Processos e Serviços
				1. Definição
				1. Operações
            1. Configurações de Sistema
				1. Rede
				1. Usuários e Grupos
			1. Pacotes
				1. Busca
				1. Instalação
				1. Remoção
				1. Atualização
      1. Permissões e Propriedades
			1. Usuários e Grupos
            1. Arquivos e Diretórios