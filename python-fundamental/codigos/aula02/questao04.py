'''
Questão04
Sabendo que str() converte valores numéricos para string,
calcule quantos dígitos há em 2 elevado a um milhão.
'''
print('Conta digitos de 2^1000000')

valor = 2**1000000 # cálcula o valor
valor = str(valor) # converte para string o resultado

numero_de_digitos = len(valor) # len() retorna número de elementos em sequências

print('Quantidade de digitos:',numero_de_digitos)
